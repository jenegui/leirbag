<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
//use AuthenticatesUsers;
class LoginController extends Controller
{
     public function login()
    {
        $credentials=$this->validate(request(), [
            'email' => 'email|required|string', 
            'password' => 'required|string'
        ]);

        //return $credentials;
        if(Auth::attempt($credentials))
         {
            //dd("$datosusuarios");
           // protected $redirectTo = '/index';
            
            return redirect()->intended('inicio'); 
            
         }
         else
         {
            return back()->withErrors(['email'=>'Las credenciales ingresadas no concuerdan con nuestros registros.']);
            return back()
            ->withErrors(['email'=> trans('auth.failed')])
            ->withInput(request(['email']));
        }

    }

    
}
