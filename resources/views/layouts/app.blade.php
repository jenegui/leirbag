<!DOCTYPE html>
<html>
<head>
	<title>Login personalizado</title>
	<link rel="stylesheet" href="/css/app.css">
</head>
<body>
	<div class="container">
		<hr>
	    @yield('content')	
	</div>
</body>
</html>