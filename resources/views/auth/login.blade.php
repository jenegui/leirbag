@extends('layouts.app')

@section('content')
	<div class="container">
	    <div class="row justify-content-center">
	        <div class="col-md-8">
				<div class="card">
					<div class="panel-heading">
						<div class="card-header">{{ __('Login') }}</div>
					</div>
					<div class="panel-body">
						<form method="POST" action="{{ route('ingresar') }}">
							{{ csrf_field() }}

							<div class="form-group row {{ $errors->has('email')?'has-error':'' }}">
								<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo electrónico') }}</label>
									<div class="col-md-6">
										<input class="form-control"
										type="email" 
										name="email"
										value="{{ old('email') }}" 
										placeholder="Ingresa tu email">
										{!! $errors->first('email', '<span class="help-block">:message</span>') !!}
									</div>	
							</div>
							<div class="form-group row {{ $errors->has('password')?'has-error':'' }}">
								<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
								<div class="col-md-6">
									<input class="form-control"
									type="password" 
									name="password" 
									placeholder="Ingresa tu password">
									{!! $errors->first('password', '<span class="help-block">:message</span>') !!}
								</div>
							</div>
							<div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Acceder') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>	
						</form>
					</div>

				</div>	
			</div>
		</div>
	</div>	

@endsection